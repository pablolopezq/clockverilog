module Clock (
    input reset,
    input clk,
    input hora_btn,
    output[2:0] red,
    output[2:0] green,
    output[1:0] blue,
    output reg hsync,
    output reg vsync
    );

reg [9:0] vcount;
reg [10:0] hcount;
reg [7:0] rgb;

reg [5:0] sec;
reg [3:0] ones;
reg [2:0] tens; 
reg [3:0] hour;
reg [18:0] counter;

reg [7:0] color;
reg [7:0] color2;
reg [7:0] color3;
reg [7:0] color4;
reg [7:0] color5;

reg [15:0] address;
wire [7:0] img_pixel; 

reg [15:0] address2;
wire [7:0] img_pixel2;

reg [15:0] address3;
wire [7:0] img_pixel3;

reg [15:0] address4;
wire [7:0] img_pixel4;

reg [15:0] address5;
wire [7:0] img_pixel5;

assign red = rgb[7:5];
assign green = rgb[4:2];
assign blue = rgb[1:0];

assign color = img_pixel;
assign color2 = img_pixel2;
assign color3 = img_pixel3;
assign color4 = img_pixel4;
assign color5 = img_pixel5;

ImageROM romimage(
.address(address),
.data(img_pixel)
);

ImageROM romimage2(
.address(address2),
.data(img_pixel2)
);

ImageROM romimage3(
.address(address3),
.data(img_pixel3)
);

ImageROM romimage4(
.address(address4),
.data(img_pixel4)
);

ImageROM romimage5(
.address(address5),
.data(img_pixel5)
);

reg [15:0] firstPos;
//assign firstPos = 16'd0;

reg [15:0] secondPos;
//assign secondPos = 16'd60;

reg [15:0] fourthPos;
//assign fourthPos = 16'd180;

reg [15:0] fifthPos;
//assign fifthPos = 16'd29040;

//Sync
always @(posedge clk) begin
    if (vcount >= 10'd601 && vcount < 10'd605)
    begin
        vsync = 1'd0;
    end
    else begin
        vsync = 1'd1;
    end

    if (hcount >= 11'd840 && hcount < 11'd968)
    begin
        hsync = 1'd0;
    end
    else
    begin
        hsync = 1'd1;
    end
end

always @ (posedge clk)
begin
    if(hora_btn) begin
        hour <= hour + 4'd1;
    end
end

//Case de logica que va contando los segundos, minutos, y horas
always @ (posedge clk)
begin
    if(counter == 20'd300000)begin
	//$display("%d",counter);        
	if(sec < 6'd59)begin
            sec <= sec + 6'd1;
            //$display("%d",sec);
        end      
        else begin
            sec <= 6'd0;
            if(ones < 4'd9)begin
                ones <= ones + 6'd1;
                //$display("%d",min);
            end
            else begin
                ones <= 6'd0;
                if(tens < 3'd5)
                    tens <= tens + 4'd1;
                else
                    tens <= 4'd0;
                    if(hour < 4'd12)
                        hour <= hour + 4'd1;
                    else
                        hour <= 4'd0;
            end
        end
        counter <= 20'd0;
    end
    else begin
        counter <= counter + 20'd1;
    end
end

//Case para aumentar la hora
always @ (posedge clk)
begin
    case(hour)
	4'd0:begin
	    firstPos <= 16'd0;
	    secondPos <= 16'd60;
	end
	4'd1:begin
	    firstPos <= 16'd29040;
	    secondPos <= 16'd0;
	end
	4'd2:begin
	    firstPos <= 16'd29040;
	    secondPos <= 16'd60;
	end
	4'd3:begin
	    firstPos <= 16'd29040;
	    secondPos <= 16'd120;
	end
	4'd4:begin
	    firstPos <= 16'd29040;
	    secondPos <= 16'd180;
	end
	4'd5:begin
	    firstPos <= 16'd29040;
	    secondPos <= 16'd240;
	end
	4'd6:begin
	    firstPos <= 16'd29040;
	    secondPos <= 16'd28800;
	end
	4'd7:begin
	    firstPos <= 16'd29040;
	    secondPos <= 16'd28860;
	end
	4'd8:begin
	    firstPos <= 16'd29040;
	    secondPos <= 16'd28920;
	end
	4'd9:begin
	    firstPos <= 16'd29040;
	    secondPos <= 16'd28980;
	end
	4'd10:begin
	    firstPos <= 16'd0;
	    secondPos <= 16'd29040;
	end
	4'd11:begin
	    firstPos <= 16'd29040;
	    secondPos <= 16'd29040;
	end	  	  	  	  	  	  	  	  	  	  	  	  	  	  	   
   endcase
end

//Case para aumentar 5to digito
always @ (posedge clk)
begin
    case(ones)
        4'd0: fifthPos <= 16'd29040;
        4'd1: fifthPos <= 16'd0;
        4'd2: fifthPos <= 16'd60;
        4'd3: fifthPos <= 16'd120;
        4'd4: fifthPos <= 16'd180;
        4'd5: fifthPos <= 16'd240;
        4'd6: fifthPos <= 16'd28800;
        4'd7: fifthPos <= 16'd28860;
        4'd8: fifthPos <= 16'd28920;
        4'd9: fifthPos <= 16'd28980;
    endcase
end

//Case para aumentar 4to digito
always @ (posedge clk)
begin
    case(tens)
        3'd0: fourthPos <= 16'd29040;
        3'd1: fourthPos <= 16'd0;
        3'd2: fourthPos <= 16'd60;
        3'd3: fourthPos <= 16'd120;
        3'd4: fourthPos <= 16'd180;
        3'd5: fourthPos <= 16'd240;
    endcase
end

always @ (posedge clk)
begin
    if (address >= 16'd57599) begin
        address <= firstPos;
    end

    if (address2 >= 16'd57599) begin
        address2 <= secondPos;
    end

    if (address3 >= 16'd57599) begin
        address3 <= 16'd300;
    end

    if (address4 >= 16'd57599) begin
        address4 <= fourthPos;
    end

    if (address5 >= 16'd57599) begin
        address5 <= fifthPos;
    end
end

//Case de logica que dibuja en pantalla
always @ (posedge clk)
begin

    if (hcount == 11'd1055)
    begin   
        hcount <= 11'd0;

        if (vcount == 10'd627) begin
            vcount <= 10'd0;

        end else begin
            vcount <= vcount + 10'd1;
        end
    end else begin
        hcount <= hcount + 11'd1;
    end

    if (hcount < 11'd800 && vcount < 10'd600) begin
        if(vcount >= 10'd0 && vcount < 10'd160 && hcount >= 11'd0 && hcount < 11'd360) begin
            address <= address + 16'd1;
            if(vcount >= 10'd0 && vcount < 10'd80 && hcount >= 11'd0 && hcount < 11'd60)
                rgb <= color;
        end 
        if(vcount >= 10'd0 && vcount < 10'd160 && hcount >= 11'd60 && hcount < 11'd420) begin
            address2 <= address2 + 16'd1;
            if(vcount >= 10'd0 && vcount < 10'd80 && hcount >= 11'd60 && hcount < 11'd120)
                rgb <= color2;
        end
        if(vcount >= 10'd0 && vcount < 10'd160 && hcount >= 11'd120 && hcount < 11'd480) begin
            address3 <= address3 + 16'd1;
            if(vcount >= 10'd0 && vcount < 10'd80 && hcount >= 11'd120 && hcount < 11'd180)
                rgb <= color3;
        end
        if(vcount >= 10'd0 && vcount < 10'd160 && hcount >= 11'd180 && hcount < 11'd540) begin
            address4 <= address4 + 16'd1;
            //$display("%d",fourthPos);
            if(vcount >= 10'd0 && vcount < 10'd80 && hcount >= 11'd180 && hcount < 11'd240)
                rgb <= color4;
        end
        if(vcount >= 10'd0 && vcount < 10'd160 && hcount >= 11'd240 && hcount < 11'd600) begin
            address5 <= address5 + 16'd1;
            $display("%d",fifthPos);
            if(vcount >= 10'd0 && vcount < 10'd80 && hcount >= 11'd240 && hcount < 11'd300)
                rgb <= color5;
        end
    end else
       rgb <= 8'hFF;
end

//Reset (Obligatorio)
always @ (posedge clk)
begin
    if(reset)begin     
        vcount <= 10'd0;
        hcount <= 11'd0;
        vsync = 1'd0;
        hsync = 1'd0;
        sec <= 6'd0;
        ones <= 4'd9;
        tens <= 3'd0;
        hour <= 4'd0;
        counter <= 20'd0;
    end
end

endmodule

//Case para aumentar los minutos
// always @ (*)
// begin
//     case(min)
//         6'd0:begin
//             fourthPos <= 16'd29040;
//             fifthPos <= 16'd29040;
//         end
//         6'd1:begin
//             fourthPos <= 16'd29040;
//             fifthPos <= 16'd0;
//         end
//         6'd2:begin
//             fourthPos <= 16'd29040;
//             fifthPos <= 16'd60;
//         end
//         6'd3:begin
//             fourthPos <= 16'd29040;
//             fifthPos <= 16'd120;
//         end
//         6'd4:begin
//             fourthPos <= 16'd29040;
//             fifthPos <= 16'd180;
//         end
//         6'd5:begin
//             fourthPos <= 16'd29040;
//             fifthPos <= 16'd240;
//         end
//         6'd6:begin
//             fourthPos <= 16'd29040;
//             fifthPos <= 16'd28800;
//         end
//         6'd7:begin
//             fourthPos <= 16'd29040;
//             fifthPos <= 16'd28860;
//         end
//         6'd8:begin
//             fourthPos <= 16'd29040;
//             fifthPos <= 16'd28920;
//         end
//         6'd9:begin
//             fourthPos <= 16'd29040;
//             fifthPos <= 16'd28980;
//         end
//         6'd10:begin
//             fourthPos <= 16'd0;
//             fifthPos <= 16'd29040;
//         end
//         6'd11:begin
//             fourthPos <= 16'd0;
//             fifthPos <= 16'd0;
//         end
//         6'd12:begin
//             fourthPos <= 16'd0;
//             fifthPos <= 16'd60;
//         end
//         6'd13:begin
//             fourthPos <= 16'd0;
//             fifthPos <= 16'd120;  
//         end
//         6'd14:begin
//             fourthPos <= 16'd0;
//             fifthPos <= 16'd180;
//         end
//         6'd15:begin
//             fourthPos <= 16'd0;
//             fifthPos <= 16'd240;
//         end
//         6'd16:begin
//             fourthPos <= 16'd0;
//             fifthPos <= 16'd28800;
//         end
//         6'd17:begin
//             fourthPos <= 16'd0;
//             fifthPos <= 16'd28860;
//         end
//         6'd18:begin
//             fourthPos <= 16'd0;
//             fifthPos <= 16'd28920;
//         end
//         6'd19:begin
//             fourthPos <= 16'd0;
//             fifthPos <= 16'd28980;
//         end
//         6'd20:begin
//             fourthPos <= 16'd60;
//             fifthPos <= 16'd29040;
//         end
//         6'd21:begin
//             fourthPos <= 16'd60;
//             fifthPos <= 16'd0;
//         end
//         6'd22:begin
//             fourthPos <= 16'd60;
//             fifthPos <= 16'd60;
//         end
//         6'd23:begin
//             fourthPos <= 16'd60;
//             fifthPos <= 16'd120;
//         end
//         6'd24:begin
//             fourthPos <= 16'd60;
//             fifthPos <= 16'd180;
//         end
//         6'd25:begin
//             fourthPos <= 16'd60;
//             fifthPos <= 16'd240;
//         end
//         6'd26:begin
//             fourthPos <= 16'd60;
//             fifthPos <= 16'd28800;
//         end
//         6'd27:begin
//             fourthPos <= 16'd60;
//             fifthPos <= 16'd28860;
//         end
//         6'd28:begin
//             fourthPos <= 16'd60;
//             fifthPos <= 16'd28920;
//         end
//         6'd29:begin
//             fourthPos <= 16'd60;
//             fifthPos <= 16'd28980;
//         end
//         6'd30:begin
//             fourthPos <= 16'd120;
//             fifthPos <= 16'd29040;
//         end
//         6'd31:begin
//             fourthPos <= 16'd120;
//             fifthPos <= 16'd0;
//         end
//         6'd32:begin
//             fourthPos <= 16'd120;
//             fifthPos <= 16'd60;
//         end
//         6'd33:begin
//             fourthPos <= 16'd120;
//             fifthPos <= 16'd120;
//         end
//         6'd34:begin
//             fourthPos <= 16'd120;
//             fifthPos <= 16'd180;
//         end
//         6'd35:begin
//             fourthPos <= 16'd120;
//             fifthPos <= 16'd240;
//         end
//         6'd36:begin
//             fourthPos <= 16'd120;
//             fifthPos <= 16'd28800;
//         end
//         6'd37:begin
//             fourthPos <= 16'd120;
//             fifthPos <= 16'd28860;
//         end
//         6'd38:begin
//             fourthPos <= 16'd120;
//             fifthPos <= 16'd28920;
//         end
//         6'd39:begin
//             fourthPos <= 16'd120;
//             fifthPos <= 16'd28980;
//         end
//         6'd40:begin
//             fourthPos <= 16'd180;
//             fifthPos <= 16'd29040;
//         end
//         6'd41:begin
//             fourthPos <= 16'd180;
//             fifthPos <= 16'd0;
//         end
//         6'd42:begin
//             fourthPos <= 16'd180;
//             fifthPos <= 16'd60;
//         end
//         6'd43:begin
//             fourthPos <= 16'd180;
//             fifthPos <= 16'd120;
//         end
//         6'd44:begin
//             fourthPos <= 16'd180;
//             fifthPos <= 16'd180;
//         end
//         6'd45:begin
//             fourthPos <= 16'd180;
//             fifthPos <= 16'd240;
//         end
//         6'd46:begin
//             fourthPos <= 16'd180;
//             fifthPos <= 16'd28800;
//         end
//         6'd47:begin
//             fourthPos <= 16'd180;
//             fifthPos <= 16'd28860;
//         end
//         6'd48:begin
//             fourthPos <= 16'd180;
//             fifthPos <= 16'd28920;
//         end
//         6'd49:begin
//             fourthPos <= 16'd180;
//             fifthPos <= 16'd28980;
//         end
//         6'd50:begin
//             fourthPos <= 16'd240;
//             fifthPos <= 16'd29040;
//         end
//         6'd51:begin
//             fourthPos <= 16'd240;
//             fifthPos <= 16'd0;
//         end
//         6'd52:begin
//             fourthPos <= 16'd240;
//             fifthPos <= 16'd60;
//         end
//         6'd53:begin
//             fourthPos <= 16'd240;
//             fifthPos <= 16'd120;
//         end
//         6'd54:begin
//             fourthPos <= 16'd240;
//             fifthPos <= 16'd180;
//         end
//         6'd55:begin
//             fourthPos <= 16'd240;
//             fifthPos <= 16'd240;
//         end
//         6'd56:begin
//             fourthPos <= 16'd240;
//             fifthPos <= 16'd28800;
//         end
//         6'd57:begin
//             fourthPos <= 16'd240;
//             fifthPos <= 16'd28860;
//         end
//         6'd58:begin
//             fourthPos <= 16'd240;
//             fifthPos <= 16'd28920;
//         end
//         6'd59:begin
//             fourthPos <= 16'd240;
//             fifthPos <= 16'd28980;
//         end
//         default: begin
//             fourthPos <= 16'd0;
//             fifthPos <= 16'd0;
//         end
//     endcase
// end