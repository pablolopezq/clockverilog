module ImageROM (
    input [15:0] address,
    output reg [7:0] data
);
reg [7:0] rom_content[0:57599];

always @ (address)
	data = rom_content[address];

initial begin
	$readmemh("/home/pablo/Documents/Clases/Diseno/Proyecto/vsrc/numbers.mif", rom_content, 0, 57599);
end
endmodule