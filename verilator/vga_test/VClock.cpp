// Verilated -*- C++ -*-
// DESCRIPTION: Verilator output: Design implementation internals
// See VClock.h for the primary calling header

#include "VClock.h"            // For This
#include "VClock__Syms.h"

//--------------------
// STATIC VARIABLES

VL_ST_SIG16(VClock::__Vtable1_v__DOT__firstPos[16],15,0);
VL_ST_SIG16(VClock::__Vtable1_v__DOT__secondPos[16],15,0);
VL_ST_SIG16(VClock::__Vtable2_v__DOT__fifthPos[16],15,0);
VL_ST_SIG16(VClock::__Vtable3_v__DOT__fourthPos[8],15,0);

//--------------------

VL_CTOR_IMP(VClock) {
    VClock__Syms* __restrict vlSymsp = __VlSymsp = new VClock__Syms(this, name());
    VClock* __restrict vlTOPp VL_ATTR_UNUSED = vlSymsp->TOPp;
    // Reset internal values
    
    // Reset structure values
    reset = VL_RAND_RESET_I(1);
    clk = VL_RAND_RESET_I(1);
    hora_btn = VL_RAND_RESET_I(1);
    red = VL_RAND_RESET_I(3);
    green = VL_RAND_RESET_I(3);
    blue = VL_RAND_RESET_I(2);
    hsync = VL_RAND_RESET_I(1);
    vsync = VL_RAND_RESET_I(1);
    v__DOT__vcount = VL_RAND_RESET_I(10);
    v__DOT__hcount = VL_RAND_RESET_I(11);
    v__DOT__rgb = VL_RAND_RESET_I(8);
    v__DOT__sec = VL_RAND_RESET_I(6);
    v__DOT__ones = VL_RAND_RESET_I(4);
    v__DOT__tens = VL_RAND_RESET_I(3);
    v__DOT__hour = VL_RAND_RESET_I(4);
    v__DOT__counter = VL_RAND_RESET_I(19);
    v__DOT__address = VL_RAND_RESET_I(16);
    v__DOT__img_pixel = VL_RAND_RESET_I(8);
    v__DOT__address2 = VL_RAND_RESET_I(16);
    v__DOT__img_pixel2 = VL_RAND_RESET_I(8);
    v__DOT__address3 = VL_RAND_RESET_I(16);
    v__DOT__img_pixel3 = VL_RAND_RESET_I(8);
    v__DOT__address4 = VL_RAND_RESET_I(16);
    v__DOT__img_pixel4 = VL_RAND_RESET_I(8);
    v__DOT__address5 = VL_RAND_RESET_I(16);
    v__DOT__img_pixel5 = VL_RAND_RESET_I(8);
    v__DOT__firstPos = VL_RAND_RESET_I(16);
    v__DOT__secondPos = VL_RAND_RESET_I(16);
    v__DOT__fourthPos = VL_RAND_RESET_I(16);
    v__DOT__fifthPos = VL_RAND_RESET_I(16);
    { int __Vi0=0; for (; __Vi0<57600; ++__Vi0) {
	    v__DOT__romimage__DOT__rom_content[__Vi0] = VL_RAND_RESET_I(8);
    }}
    { int __Vi0=0; for (; __Vi0<57600; ++__Vi0) {
	    v__DOT__romimage2__DOT__rom_content[__Vi0] = VL_RAND_RESET_I(8);
    }}
    { int __Vi0=0; for (; __Vi0<57600; ++__Vi0) {
	    v__DOT__romimage3__DOT__rom_content[__Vi0] = VL_RAND_RESET_I(8);
    }}
    { int __Vi0=0; for (; __Vi0<57600; ++__Vi0) {
	    v__DOT__romimage4__DOT__rom_content[__Vi0] = VL_RAND_RESET_I(8);
    }}
    { int __Vi0=0; for (; __Vi0<57600; ++__Vi0) {
	    v__DOT__romimage5__DOT__rom_content[__Vi0] = VL_RAND_RESET_I(8);
    }}
    __Vtablechg1[0] = 3U;
    __Vtablechg1[1] = 3U;
    __Vtablechg1[2] = 3U;
    __Vtablechg1[3] = 3U;
    __Vtablechg1[4] = 3U;
    __Vtablechg1[5] = 3U;
    __Vtablechg1[6] = 3U;
    __Vtablechg1[7] = 3U;
    __Vtablechg1[8] = 3U;
    __Vtablechg1[9] = 3U;
    __Vtablechg1[10] = 3U;
    __Vtablechg1[11] = 3U;
    __Vtablechg1[12] = 0U;
    __Vtablechg1[13] = 0U;
    __Vtablechg1[14] = 0U;
    __Vtablechg1[15] = 0U;
    __Vtable1_v__DOT__firstPos[0] = 0U;
    __Vtable1_v__DOT__firstPos[1] = 0x7170U;
    __Vtable1_v__DOT__firstPos[2] = 0x7170U;
    __Vtable1_v__DOT__firstPos[3] = 0x7170U;
    __Vtable1_v__DOT__firstPos[4] = 0x7170U;
    __Vtable1_v__DOT__firstPos[5] = 0x7170U;
    __Vtable1_v__DOT__firstPos[6] = 0x7170U;
    __Vtable1_v__DOT__firstPos[7] = 0x7170U;
    __Vtable1_v__DOT__firstPos[8] = 0x7170U;
    __Vtable1_v__DOT__firstPos[9] = 0x7170U;
    __Vtable1_v__DOT__firstPos[10] = 0U;
    __Vtable1_v__DOT__firstPos[11] = 0x7170U;
    __Vtable1_v__DOT__firstPos[12] = 0U;
    __Vtable1_v__DOT__firstPos[13] = 0U;
    __Vtable1_v__DOT__firstPos[14] = 0U;
    __Vtable1_v__DOT__firstPos[15] = 0U;
    __Vtable1_v__DOT__secondPos[0] = 0x3cU;
    __Vtable1_v__DOT__secondPos[1] = 0U;
    __Vtable1_v__DOT__secondPos[2] = 0x3cU;
    __Vtable1_v__DOT__secondPos[3] = 0x78U;
    __Vtable1_v__DOT__secondPos[4] = 0xb4U;
    __Vtable1_v__DOT__secondPos[5] = 0xf0U;
    __Vtable1_v__DOT__secondPos[6] = 0x7080U;
    __Vtable1_v__DOT__secondPos[7] = 0x70bcU;
    __Vtable1_v__DOT__secondPos[8] = 0x70f8U;
    __Vtable1_v__DOT__secondPos[9] = 0x7134U;
    __Vtable1_v__DOT__secondPos[10] = 0x7170U;
    __Vtable1_v__DOT__secondPos[11] = 0x7170U;
    __Vtable1_v__DOT__secondPos[12] = 0U;
    __Vtable1_v__DOT__secondPos[13] = 0U;
    __Vtable1_v__DOT__secondPos[14] = 0U;
    __Vtable1_v__DOT__secondPos[15] = 0U;
    __Vtablechg2[0] = 1U;
    __Vtablechg2[1] = 1U;
    __Vtablechg2[2] = 1U;
    __Vtablechg2[3] = 1U;
    __Vtablechg2[4] = 1U;
    __Vtablechg2[5] = 1U;
    __Vtablechg2[6] = 1U;
    __Vtablechg2[7] = 1U;
    __Vtablechg2[8] = 1U;
    __Vtablechg2[9] = 1U;
    __Vtablechg2[10] = 0U;
    __Vtablechg2[11] = 0U;
    __Vtablechg2[12] = 0U;
    __Vtablechg2[13] = 0U;
    __Vtablechg2[14] = 0U;
    __Vtablechg2[15] = 0U;
    __Vtable2_v__DOT__fifthPos[0] = 0x7170U;
    __Vtable2_v__DOT__fifthPos[1] = 0U;
    __Vtable2_v__DOT__fifthPos[2] = 0x3cU;
    __Vtable2_v__DOT__fifthPos[3] = 0x78U;
    __Vtable2_v__DOT__fifthPos[4] = 0xb4U;
    __Vtable2_v__DOT__fifthPos[5] = 0xf0U;
    __Vtable2_v__DOT__fifthPos[6] = 0x7080U;
    __Vtable2_v__DOT__fifthPos[7] = 0x70bcU;
    __Vtable2_v__DOT__fifthPos[8] = 0x70f8U;
    __Vtable2_v__DOT__fifthPos[9] = 0x7134U;
    __Vtable2_v__DOT__fifthPos[10] = 0U;
    __Vtable2_v__DOT__fifthPos[11] = 0U;
    __Vtable2_v__DOT__fifthPos[12] = 0U;
    __Vtable2_v__DOT__fifthPos[13] = 0U;
    __Vtable2_v__DOT__fifthPos[14] = 0U;
    __Vtable2_v__DOT__fifthPos[15] = 0U;
    __Vtablechg3[0] = 1U;
    __Vtablechg3[1] = 1U;
    __Vtablechg3[2] = 1U;
    __Vtablechg3[3] = 1U;
    __Vtablechg3[4] = 1U;
    __Vtablechg3[5] = 1U;
    __Vtablechg3[6] = 0U;
    __Vtablechg3[7] = 0U;
    __Vtable3_v__DOT__fourthPos[0] = 0x7170U;
    __Vtable3_v__DOT__fourthPos[1] = 0U;
    __Vtable3_v__DOT__fourthPos[2] = 0x3cU;
    __Vtable3_v__DOT__fourthPos[3] = 0x78U;
    __Vtable3_v__DOT__fourthPos[4] = 0xb4U;
    __Vtable3_v__DOT__fourthPos[5] = 0xf0U;
    __Vtable3_v__DOT__fourthPos[6] = 0U;
    __Vtable3_v__DOT__fourthPos[7] = 0U;
    __Vclklast__TOP__clk = VL_RAND_RESET_I(1);
    __Vm_traceActivity = VL_RAND_RESET_I(32);
}

void VClock::__Vconfigure(VClock__Syms* vlSymsp, bool first) {
    if (0 && first) {}  // Prevent unused
    this->__VlSymsp = vlSymsp;
}

VClock::~VClock() {
    delete __VlSymsp; __VlSymsp=NULL;
}

//--------------------


void VClock::eval() {
    VClock__Syms* __restrict vlSymsp = this->__VlSymsp; // Setup global symbol table
    VClock* __restrict vlTOPp VL_ATTR_UNUSED = vlSymsp->TOPp;
    // Initialize
    if (VL_UNLIKELY(!vlSymsp->__Vm_didInit)) _eval_initial_loop(vlSymsp);
    // Evaluate till stable
    VL_DEBUG_IF(VL_PRINTF("\n----TOP Evaluate VClock::eval\n"); );
    int __VclockLoop = 0;
    QData __Vchange=1;
    while (VL_LIKELY(__Vchange)) {
	VL_DEBUG_IF(VL_PRINTF(" Clock loop\n"););
	vlSymsp->__Vm_activity = true;
	_eval(vlSymsp);
	__Vchange = _change_request(vlSymsp);
	if (++__VclockLoop > 100) vl_fatal(__FILE__,__LINE__,__FILE__,"Verilated model didn't converge");
    }
}

void VClock::_eval_initial_loop(VClock__Syms* __restrict vlSymsp) {
    vlSymsp->__Vm_didInit = true;
    _eval_initial(vlSymsp);
    vlSymsp->__Vm_activity = true;
    int __VclockLoop = 0;
    QData __Vchange=1;
    while (VL_LIKELY(__Vchange)) {
	_eval_settle(vlSymsp);
	_eval(vlSymsp);
	__Vchange = _change_request(vlSymsp);
	if (++__VclockLoop > 100) vl_fatal(__FILE__,__LINE__,__FILE__,"Verilated model didn't DC converge");
    }
}

//--------------------
// Internal Methods

void VClock::_initial__TOP__1(VClock__Syms* __restrict vlSymsp) {
    VL_DEBUG_IF(VL_PRINTF("    VClock::_initial__TOP__1\n"); );
    VClock* __restrict vlTOPp VL_ATTR_UNUSED = vlSymsp->TOPp;
    // Variables
    //char	__VpadToAlign4[4];
    VL_SIGW(__Vtemp1,511,0,16);
    VL_SIGW(__Vtemp2,511,0,16);
    VL_SIGW(__Vtemp3,511,0,16);
    VL_SIGW(__Vtemp4,511,0,16);
    VL_SIGW(__Vtemp5,511,0,16);
    // Body
    // INITIAL at ../vsrc//ImageROM.v:10
    __Vtemp1[0U] = 0x2e6d6966U;
    __Vtemp1[1U] = 0x62657273U;
    __Vtemp1[2U] = 0x2f6e756dU;
    __Vtemp1[3U] = 0x76737263U;
    __Vtemp1[4U] = 0x63746f2fU;
    __Vtemp1[5U] = 0x726f7965U;
    __Vtemp1[6U] = 0x6e6f2f50U;
    __Vtemp1[7U] = 0x44697365U;
    __Vtemp1[8U] = 0x7365732fU;
    __Vtemp1[9U] = 0x2f436c61U;
    __Vtemp1[0xaU] = 0x656e7473U;
    __Vtemp1[0xbU] = 0x6f63756dU;
    __Vtemp1[0xcU] = 0x6c6f2f44U;
    __Vtemp1[0xdU] = 0x2f706162U;
    __Vtemp1[0xeU] = 0x686f6d65U;
    __Vtemp1[0xfU] = 0x2fU;
    VL_READMEM_W (true,8,57600, 0,16, __Vtemp1, vlTOPp->v__DOT__romimage__DOT__rom_content
		  ,0U,0xe0ffU);
    // INITIAL at ../vsrc//ImageROM.v:10
    __Vtemp2[0U] = 0x2e6d6966U;
    __Vtemp2[1U] = 0x62657273U;
    __Vtemp2[2U] = 0x2f6e756dU;
    __Vtemp2[3U] = 0x76737263U;
    __Vtemp2[4U] = 0x63746f2fU;
    __Vtemp2[5U] = 0x726f7965U;
    __Vtemp2[6U] = 0x6e6f2f50U;
    __Vtemp2[7U] = 0x44697365U;
    __Vtemp2[8U] = 0x7365732fU;
    __Vtemp2[9U] = 0x2f436c61U;
    __Vtemp2[0xaU] = 0x656e7473U;
    __Vtemp2[0xbU] = 0x6f63756dU;
    __Vtemp2[0xcU] = 0x6c6f2f44U;
    __Vtemp2[0xdU] = 0x2f706162U;
    __Vtemp2[0xeU] = 0x686f6d65U;
    __Vtemp2[0xfU] = 0x2fU;
    VL_READMEM_W (true,8,57600, 0,16, __Vtemp2, vlTOPp->v__DOT__romimage2__DOT__rom_content
		  ,0U,0xe0ffU);
    // INITIAL at ../vsrc//ImageROM.v:10
    __Vtemp3[0U] = 0x2e6d6966U;
    __Vtemp3[1U] = 0x62657273U;
    __Vtemp3[2U] = 0x2f6e756dU;
    __Vtemp3[3U] = 0x76737263U;
    __Vtemp3[4U] = 0x63746f2fU;
    __Vtemp3[5U] = 0x726f7965U;
    __Vtemp3[6U] = 0x6e6f2f50U;
    __Vtemp3[7U] = 0x44697365U;
    __Vtemp3[8U] = 0x7365732fU;
    __Vtemp3[9U] = 0x2f436c61U;
    __Vtemp3[0xaU] = 0x656e7473U;
    __Vtemp3[0xbU] = 0x6f63756dU;
    __Vtemp3[0xcU] = 0x6c6f2f44U;
    __Vtemp3[0xdU] = 0x2f706162U;
    __Vtemp3[0xeU] = 0x686f6d65U;
    __Vtemp3[0xfU] = 0x2fU;
    VL_READMEM_W (true,8,57600, 0,16, __Vtemp3, vlTOPp->v__DOT__romimage3__DOT__rom_content
		  ,0U,0xe0ffU);
    // INITIAL at ../vsrc//ImageROM.v:10
    __Vtemp4[0U] = 0x2e6d6966U;
    __Vtemp4[1U] = 0x62657273U;
    __Vtemp4[2U] = 0x2f6e756dU;
    __Vtemp4[3U] = 0x76737263U;
    __Vtemp4[4U] = 0x63746f2fU;
    __Vtemp4[5U] = 0x726f7965U;
    __Vtemp4[6U] = 0x6e6f2f50U;
    __Vtemp4[7U] = 0x44697365U;
    __Vtemp4[8U] = 0x7365732fU;
    __Vtemp4[9U] = 0x2f436c61U;
    __Vtemp4[0xaU] = 0x656e7473U;
    __Vtemp4[0xbU] = 0x6f63756dU;
    __Vtemp4[0xcU] = 0x6c6f2f44U;
    __Vtemp4[0xdU] = 0x2f706162U;
    __Vtemp4[0xeU] = 0x686f6d65U;
    __Vtemp4[0xfU] = 0x2fU;
    VL_READMEM_W (true,8,57600, 0,16, __Vtemp4, vlTOPp->v__DOT__romimage4__DOT__rom_content
		  ,0U,0xe0ffU);
    // INITIAL at ../vsrc//ImageROM.v:10
    __Vtemp5[0U] = 0x2e6d6966U;
    __Vtemp5[1U] = 0x62657273U;
    __Vtemp5[2U] = 0x2f6e756dU;
    __Vtemp5[3U] = 0x76737263U;
    __Vtemp5[4U] = 0x63746f2fU;
    __Vtemp5[5U] = 0x726f7965U;
    __Vtemp5[6U] = 0x6e6f2f50U;
    __Vtemp5[7U] = 0x44697365U;
    __Vtemp5[8U] = 0x7365732fU;
    __Vtemp5[9U] = 0x2f436c61U;
    __Vtemp5[0xaU] = 0x656e7473U;
    __Vtemp5[0xbU] = 0x6f63756dU;
    __Vtemp5[0xcU] = 0x6c6f2f44U;
    __Vtemp5[0xdU] = 0x2f706162U;
    __Vtemp5[0xeU] = 0x686f6d65U;
    __Vtemp5[0xfU] = 0x2fU;
    VL_READMEM_W (true,8,57600, 0,16, __Vtemp5, vlTOPp->v__DOT__romimage5__DOT__rom_content
		  ,0U,0xe0ffU);
}

VL_INLINE_OPT void VClock::_sequent__TOP__2(VClock__Syms* __restrict vlSymsp) {
    VL_DEBUG_IF(VL_PRINTF("    VClock::_sequent__TOP__2\n"); );
    VClock* __restrict vlTOPp VL_ATTR_UNUSED = vlSymsp->TOPp;
    // Variables
    VL_SIG8(__Vtableidx1,3,0);
    VL_SIG8(__Vtableidx2,3,0);
    VL_SIG8(__Vtableidx3,2,0);
    VL_SIG8(__Vdly__v__DOT__hour,3,0);
    VL_SIG8(__Vdly__v__DOT__sec,5,0);
    VL_SIG8(__Vdly__v__DOT__ones,3,0);
    VL_SIG8(__Vdly__v__DOT__tens,2,0);
    //char	__VpadToAlign355[1];
    VL_SIG16(__Vdly__v__DOT__address,15,0);
    VL_SIG16(__Vdly__v__DOT__address2,15,0);
    VL_SIG16(__Vdly__v__DOT__address3,15,0);
    VL_SIG16(__Vdly__v__DOT__address4,15,0);
    VL_SIG16(__Vdly__v__DOT__address5,15,0);
    VL_SIG16(__Vdly__v__DOT__vcount,9,0);
    VL_SIG16(__Vdly__v__DOT__hcount,10,0);
    //char	__VpadToAlign370[2];
    VL_SIG(__Vdly__v__DOT__counter,18,0);
    // Body
    __Vdly__v__DOT__sec = vlTOPp->v__DOT__sec;
    __Vdly__v__DOT__ones = vlTOPp->v__DOT__ones;
    __Vdly__v__DOT__tens = vlTOPp->v__DOT__tens;
    __Vdly__v__DOT__counter = vlTOPp->v__DOT__counter;
    __Vdly__v__DOT__hour = vlTOPp->v__DOT__hour;
    __Vdly__v__DOT__address3 = vlTOPp->v__DOT__address3;
    __Vdly__v__DOT__vcount = vlTOPp->v__DOT__vcount;
    __Vdly__v__DOT__hcount = vlTOPp->v__DOT__hcount;
    __Vdly__v__DOT__address4 = vlTOPp->v__DOT__address4;
    __Vdly__v__DOT__address5 = vlTOPp->v__DOT__address5;
    __Vdly__v__DOT__address = vlTOPp->v__DOT__address;
    __Vdly__v__DOT__address2 = vlTOPp->v__DOT__address2;
    // ALWAYS at ../vsrc//../vsrc/Clock.v:91
    vlTOPp->vsync = (1U & (~ ((0x259U <= (IData)(vlTOPp->v__DOT__vcount)) 
			      & (0x25dU > (IData)(vlTOPp->v__DOT__vcount)))));
    vlTOPp->hsync = (1U & (~ ((0x348U <= (IData)(vlTOPp->v__DOT__hcount)) 
			      & (0x3c8U > (IData)(vlTOPp->v__DOT__hcount)))));
    // ALWAYS at ../vsrc//../vsrc/Clock.v:110
    if (vlTOPp->hora_btn) {
	__Vdly__v__DOT__hour = (0xfU & ((IData)(1U) 
					+ (IData)(vlTOPp->v__DOT__hour)));
    }
    // ALWAYS at ../vsrc//../vsrc/Clock.v:118
    if ((0x493e0U == vlTOPp->v__DOT__counter)) {
	if ((0x3bU > (IData)(vlTOPp->v__DOT__sec))) {
	    __Vdly__v__DOT__sec = (0x3fU & ((IData)(1U) 
					    + (IData)(vlTOPp->v__DOT__sec)));
	} else {
	    if ((9U > (IData)(vlTOPp->v__DOT__ones))) {
		__Vdly__v__DOT__ones = (0xfU & ((IData)(1U) 
						+ (IData)(vlTOPp->v__DOT__ones)));
	    } else {
		__Vdly__v__DOT__tens = (7U & ((5U > (IData)(vlTOPp->v__DOT__tens))
					       ? ((IData)(1U) 
						  + (IData)(vlTOPp->v__DOT__tens))
					       : 0U));
		__Vdly__v__DOT__hour = (0xfU & ((0xcU 
						 > (IData)(vlTOPp->v__DOT__hour))
						 ? 
						((IData)(1U) 
						 + (IData)(vlTOPp->v__DOT__hour))
						 : 0U));
		__Vdly__v__DOT__ones = 0U;
	    }
	    __Vdly__v__DOT__sec = 0U;
	}
	__Vdly__v__DOT__counter = 0U;
    } else {
	__Vdly__v__DOT__counter = (0x7ffffU & ((IData)(1U) 
					       + vlTOPp->v__DOT__counter));
    }
    // ALWAYS at ../vsrc//../vsrc/Clock.v:246
    if ((0xe0ffU <= (IData)(vlTOPp->v__DOT__address3))) {
	__Vdly__v__DOT__address3 = 0x12cU;
    }
    // ALWAYS at ../vsrc//../vsrc/Clock.v:310
    if (vlTOPp->reset) {
	vlTOPp->vsync = 0U;
	__Vdly__v__DOT__vcount = 0U;
	__Vdly__v__DOT__hcount = 0U;
	__Vdly__v__DOT__sec = 0U;
	__Vdly__v__DOT__ones = 9U;
	__Vdly__v__DOT__tens = 0U;
	__Vdly__v__DOT__hour = 0U;
	__Vdly__v__DOT__counter = 0U;
	vlTOPp->hsync = 0U;
    }
    // ALWAYS at ../vsrc//../vsrc/Clock.v:250
    if ((0xe0ffU <= (IData)(vlTOPp->v__DOT__address4))) {
	__Vdly__v__DOT__address4 = vlTOPp->v__DOT__fourthPos;
    }
    // ALWAYS at ../vsrc//../vsrc/Clock.v:254
    if ((0xe0ffU <= (IData)(vlTOPp->v__DOT__address5))) {
	__Vdly__v__DOT__address5 = vlTOPp->v__DOT__fifthPos;
    }
    // ALWAYS at ../vsrc//../vsrc/Clock.v:238
    if ((0xe0ffU <= (IData)(vlTOPp->v__DOT__address))) {
	__Vdly__v__DOT__address = vlTOPp->v__DOT__firstPos;
    }
    // ALWAYS at ../vsrc//../vsrc/Clock.v:242
    if ((0xe0ffU <= (IData)(vlTOPp->v__DOT__address2))) {
	__Vdly__v__DOT__address2 = vlTOPp->v__DOT__secondPos;
    }
    // ALWAYS at ../vsrc//../vsrc/Clock.v:260
    if ((0x41fU == (IData)(vlTOPp->v__DOT__hcount))) {
	__Vdly__v__DOT__vcount = (0x3ffU & ((0x273U 
					     == (IData)(vlTOPp->v__DOT__vcount))
					     ? 0U : 
					    ((IData)(1U) 
					     + (IData)(vlTOPp->v__DOT__vcount))));
	__Vdly__v__DOT__hcount = 0U;
    } else {
	__Vdly__v__DOT__hcount = (0x7ffU & ((IData)(1U) 
					    + (IData)(vlTOPp->v__DOT__hcount)));
    }
    if (((0x320U > (IData)(vlTOPp->v__DOT__hcount)) 
	 & (0x258U > (IData)(vlTOPp->v__DOT__vcount)))) {
	if (((0xa0U > (IData)(vlTOPp->v__DOT__vcount)) 
	     & (0x168U > (IData)(vlTOPp->v__DOT__hcount)))) {
	    __Vdly__v__DOT__address = (0xffffU & ((IData)(1U) 
						  + (IData)(vlTOPp->v__DOT__address)));
	    if (((0x50U > (IData)(vlTOPp->v__DOT__vcount)) 
		 & (0x3cU > (IData)(vlTOPp->v__DOT__hcount)))) {
		vlTOPp->v__DOT__rgb = vlTOPp->v__DOT__img_pixel;
	    }
	}
	if ((((0xa0U > (IData)(vlTOPp->v__DOT__vcount)) 
	      & (0x3cU <= (IData)(vlTOPp->v__DOT__hcount))) 
	     & (0x1a4U > (IData)(vlTOPp->v__DOT__hcount)))) {
	    __Vdly__v__DOT__address2 = (0xffffU & ((IData)(1U) 
						   + (IData)(vlTOPp->v__DOT__address2)));
	    if ((((0x50U > (IData)(vlTOPp->v__DOT__vcount)) 
		  & (0x3cU <= (IData)(vlTOPp->v__DOT__hcount))) 
		 & (0x78U > (IData)(vlTOPp->v__DOT__hcount)))) {
		vlTOPp->v__DOT__rgb = vlTOPp->v__DOT__img_pixel2;
	    }
	}
	if ((((0xa0U > (IData)(vlTOPp->v__DOT__vcount)) 
	      & (0x78U <= (IData)(vlTOPp->v__DOT__hcount))) 
	     & (0x1e0U > (IData)(vlTOPp->v__DOT__hcount)))) {
	    __Vdly__v__DOT__address3 = (0xffffU & ((IData)(1U) 
						   + (IData)(vlTOPp->v__DOT__address3)));
	    if ((((0x50U > (IData)(vlTOPp->v__DOT__vcount)) 
		  & (0x78U <= (IData)(vlTOPp->v__DOT__hcount))) 
		 & (0xb4U > (IData)(vlTOPp->v__DOT__hcount)))) {
		vlTOPp->v__DOT__rgb = vlTOPp->v__DOT__img_pixel3;
	    }
	}
	if ((((0xa0U > (IData)(vlTOPp->v__DOT__vcount)) 
	      & (0xb4U <= (IData)(vlTOPp->v__DOT__hcount))) 
	     & (0x21cU > (IData)(vlTOPp->v__DOT__hcount)))) {
	    __Vdly__v__DOT__address4 = (0xffffU & ((IData)(1U) 
						   + (IData)(vlTOPp->v__DOT__address4)));
	    if ((((0x50U > (IData)(vlTOPp->v__DOT__vcount)) 
		  & (0xb4U <= (IData)(vlTOPp->v__DOT__hcount))) 
		 & (0xf0U > (IData)(vlTOPp->v__DOT__hcount)))) {
		vlTOPp->v__DOT__rgb = vlTOPp->v__DOT__img_pixel4;
	    }
	}
	if (VL_UNLIKELY((((0xa0U > (IData)(vlTOPp->v__DOT__vcount)) 
			  & (0xf0U <= (IData)(vlTOPp->v__DOT__hcount))) 
			 & (0x258U > (IData)(vlTOPp->v__DOT__hcount))))) {
	    __Vdly__v__DOT__address5 = (0xffffU & ((IData)(1U) 
						   + (IData)(vlTOPp->v__DOT__address5)));
	    VL_WRITEF("%5u\n",16,vlTOPp->v__DOT__fifthPos);
	    if ((((0x50U > (IData)(vlTOPp->v__DOT__vcount)) 
		  & (0xf0U <= (IData)(vlTOPp->v__DOT__hcount))) 
		 & (0x12cU > (IData)(vlTOPp->v__DOT__hcount)))) {
		vlTOPp->v__DOT__rgb = vlTOPp->v__DOT__img_pixel5;
	    }
	}
    } else {
	vlTOPp->v__DOT__rgb = 0xffU;
    }
    vlTOPp->v__DOT__sec = __Vdly__v__DOT__sec;
    vlTOPp->v__DOT__counter = __Vdly__v__DOT__counter;
    vlTOPp->v__DOT__hcount = __Vdly__v__DOT__hcount;
    vlTOPp->v__DOT__vcount = __Vdly__v__DOT__vcount;
    vlTOPp->v__DOT__address = __Vdly__v__DOT__address;
    vlTOPp->v__DOT__address2 = __Vdly__v__DOT__address2;
    vlTOPp->v__DOT__address3 = __Vdly__v__DOT__address3;
    vlTOPp->v__DOT__address4 = __Vdly__v__DOT__address4;
    vlTOPp->v__DOT__address5 = __Vdly__v__DOT__address5;
    // ALWAYS at ../vsrc//../vsrc/Clock.v:224
    __Vtableidx3 = vlTOPp->v__DOT__tens;
    if (vlTOPp->__Vtablechg3[__Vtableidx3]) {
	vlTOPp->v__DOT__fourthPos = vlTOPp->__Vtable3_v__DOT__fourthPos
	    [__Vtableidx3];
    }
    // ALWAYS at ../vsrc//../vsrc/Clock.v:152
    __Vtableidx1 = vlTOPp->v__DOT__hour;
    if ((1U & vlTOPp->__Vtablechg1[__Vtableidx1])) {
	vlTOPp->v__DOT__firstPos = vlTOPp->__Vtable1_v__DOT__firstPos
	    [__Vtableidx1];
    }
    if ((2U & vlTOPp->__Vtablechg1[__Vtableidx1])) {
	vlTOPp->v__DOT__secondPos = vlTOPp->__Vtable1_v__DOT__secondPos
	    [__Vtableidx1];
    }
    // ALWAYS at ../vsrc//ImageROM.v:7
    vlTOPp->v__DOT__img_pixel = ((0xe0ffU >= (IData)(vlTOPp->v__DOT__address))
				  ? vlTOPp->v__DOT__romimage__DOT__rom_content
				 [vlTOPp->v__DOT__address]
				  : 0U);
    // ALWAYS at ../vsrc//ImageROM.v:7
    vlTOPp->v__DOT__img_pixel2 = ((0xe0ffU >= (IData)(vlTOPp->v__DOT__address2))
				   ? vlTOPp->v__DOT__romimage2__DOT__rom_content
				  [vlTOPp->v__DOT__address2]
				   : 0U);
    // ALWAYS at ../vsrc//ImageROM.v:7
    vlTOPp->v__DOT__img_pixel3 = ((0xe0ffU >= (IData)(vlTOPp->v__DOT__address3))
				   ? vlTOPp->v__DOT__romimage3__DOT__rom_content
				  [vlTOPp->v__DOT__address3]
				   : 0U);
    // ALWAYS at ../vsrc//ImageROM.v:7
    vlTOPp->v__DOT__img_pixel4 = ((0xe0ffU >= (IData)(vlTOPp->v__DOT__address4))
				   ? vlTOPp->v__DOT__romimage4__DOT__rom_content
				  [vlTOPp->v__DOT__address4]
				   : 0U);
    // ALWAYS at ../vsrc//ImageROM.v:7
    vlTOPp->v__DOT__img_pixel5 = ((0xe0ffU >= (IData)(vlTOPp->v__DOT__address5))
				   ? vlTOPp->v__DOT__romimage5__DOT__rom_content
				  [vlTOPp->v__DOT__address5]
				   : 0U);
    vlTOPp->red = (7U & ((IData)(vlTOPp->v__DOT__rgb) 
			 >> 5U));
    vlTOPp->green = (7U & ((IData)(vlTOPp->v__DOT__rgb) 
			   >> 2U));
    vlTOPp->blue = (3U & (IData)(vlTOPp->v__DOT__rgb));
    // ALWAYS at ../vsrc//../vsrc/Clock.v:207
    __Vtableidx2 = vlTOPp->v__DOT__ones;
    if (vlTOPp->__Vtablechg2[__Vtableidx2]) {
	vlTOPp->v__DOT__fifthPos = vlTOPp->__Vtable2_v__DOT__fifthPos
	    [__Vtableidx2];
    }
    vlTOPp->v__DOT__tens = __Vdly__v__DOT__tens;
    vlTOPp->v__DOT__hour = __Vdly__v__DOT__hour;
    vlTOPp->v__DOT__ones = __Vdly__v__DOT__ones;
}

void VClock::_settle__TOP__3(VClock__Syms* __restrict vlSymsp) {
    VL_DEBUG_IF(VL_PRINTF("    VClock::_settle__TOP__3\n"); );
    VClock* __restrict vlTOPp VL_ATTR_UNUSED = vlSymsp->TOPp;
    // Body
    // ALWAYS at ../vsrc//ImageROM.v:7
    vlTOPp->v__DOT__img_pixel = ((0xe0ffU >= (IData)(vlTOPp->v__DOT__address))
				  ? vlTOPp->v__DOT__romimage__DOT__rom_content
				 [vlTOPp->v__DOT__address]
				  : 0U);
    // ALWAYS at ../vsrc//ImageROM.v:7
    vlTOPp->v__DOT__img_pixel2 = ((0xe0ffU >= (IData)(vlTOPp->v__DOT__address2))
				   ? vlTOPp->v__DOT__romimage2__DOT__rom_content
				  [vlTOPp->v__DOT__address2]
				   : 0U);
    // ALWAYS at ../vsrc//ImageROM.v:7
    vlTOPp->v__DOT__img_pixel3 = ((0xe0ffU >= (IData)(vlTOPp->v__DOT__address3))
				   ? vlTOPp->v__DOT__romimage3__DOT__rom_content
				  [vlTOPp->v__DOT__address3]
				   : 0U);
    // ALWAYS at ../vsrc//ImageROM.v:7
    vlTOPp->v__DOT__img_pixel4 = ((0xe0ffU >= (IData)(vlTOPp->v__DOT__address4))
				   ? vlTOPp->v__DOT__romimage4__DOT__rom_content
				  [vlTOPp->v__DOT__address4]
				   : 0U);
    // ALWAYS at ../vsrc//ImageROM.v:7
    vlTOPp->v__DOT__img_pixel5 = ((0xe0ffU >= (IData)(vlTOPp->v__DOT__address5))
				   ? vlTOPp->v__DOT__romimage5__DOT__rom_content
				  [vlTOPp->v__DOT__address5]
				   : 0U);
    vlTOPp->red = (7U & ((IData)(vlTOPp->v__DOT__rgb) 
			 >> 5U));
    vlTOPp->green = (7U & ((IData)(vlTOPp->v__DOT__rgb) 
			   >> 2U));
    vlTOPp->blue = (3U & (IData)(vlTOPp->v__DOT__rgb));
}

void VClock::_eval(VClock__Syms* __restrict vlSymsp) {
    VL_DEBUG_IF(VL_PRINTF("    VClock::_eval\n"); );
    VClock* __restrict vlTOPp VL_ATTR_UNUSED = vlSymsp->TOPp;
    // Body
    if (((IData)(vlTOPp->clk) & (~ (IData)(vlTOPp->__Vclklast__TOP__clk)))) {
	vlTOPp->_sequent__TOP__2(vlSymsp);
	vlTOPp->__Vm_traceActivity = (2U | vlTOPp->__Vm_traceActivity);
    }
    // Final
    vlTOPp->__Vclklast__TOP__clk = vlTOPp->clk;
}

void VClock::_eval_initial(VClock__Syms* __restrict vlSymsp) {
    VL_DEBUG_IF(VL_PRINTF("    VClock::_eval_initial\n"); );
    VClock* __restrict vlTOPp VL_ATTR_UNUSED = vlSymsp->TOPp;
    // Body
    vlTOPp->_initial__TOP__1(vlSymsp);
}

void VClock::final() {
    VL_DEBUG_IF(VL_PRINTF("    VClock::final\n"); );
    // Variables
    VClock__Syms* __restrict vlSymsp = this->__VlSymsp;
    VClock* __restrict vlTOPp VL_ATTR_UNUSED = vlSymsp->TOPp;
}

void VClock::_eval_settle(VClock__Syms* __restrict vlSymsp) {
    VL_DEBUG_IF(VL_PRINTF("    VClock::_eval_settle\n"); );
    VClock* __restrict vlTOPp VL_ATTR_UNUSED = vlSymsp->TOPp;
    // Body
    vlTOPp->_settle__TOP__3(vlSymsp);
    vlTOPp->__Vm_traceActivity = (1U | vlTOPp->__Vm_traceActivity);
}

VL_INLINE_OPT QData VClock::_change_request(VClock__Syms* __restrict vlSymsp) {
    VL_DEBUG_IF(VL_PRINTF("    VClock::_change_request\n"); );
    VClock* __restrict vlTOPp VL_ATTR_UNUSED = vlSymsp->TOPp;
    // Body
    // Change detection
    QData __req = false;  // Logically a bool
    return __req;
}
