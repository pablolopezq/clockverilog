// Verilated -*- C++ -*-
// DESCRIPTION: Verilator output: Tracing implementation internals
#include "verilated_vcd_c.h"
#include "VClock__Syms.h"


//======================

void VClock::traceChg(VerilatedVcd* vcdp, void* userthis, uint32_t code) {
    // Callback from vcd->dump()
    VClock* t=(VClock*)userthis;
    VClock__Syms* __restrict vlSymsp = t->__VlSymsp; // Setup global symbol table
    if (vlSymsp->getClearActivity()) {
	t->traceChgThis (vlSymsp, vcdp, code);
    }
}

//======================


void VClock::traceChgThis(VClock__Syms* __restrict vlSymsp, VerilatedVcd* vcdp, uint32_t code) {
    VClock* __restrict vlTOPp VL_ATTR_UNUSED = vlSymsp->TOPp;
    int c=code;
    if (0 && vcdp && c) {}  // Prevent unused
    // Body
    {
	if (VL_UNLIKELY((1U & (vlTOPp->__Vm_traceActivity 
			       | (vlTOPp->__Vm_traceActivity 
				  >> 1U))))) {
	    vlTOPp->traceChgThis__2(vlSymsp, vcdp, code);
	}
	if (VL_UNLIKELY((2U & vlTOPp->__Vm_traceActivity))) {
	    vlTOPp->traceChgThis__3(vlSymsp, vcdp, code);
	}
	vlTOPp->traceChgThis__4(vlSymsp, vcdp, code);
    }
    // Final
    vlTOPp->__Vm_traceActivity = 0U;
}

void VClock::traceChgThis__2(VClock__Syms* __restrict vlSymsp, VerilatedVcd* vcdp, uint32_t code) {
    VClock* __restrict vlTOPp VL_ATTR_UNUSED = vlSymsp->TOPp;
    int c=code;
    if (0 && vcdp && c) {}  // Prevent unused
    // Body
    {
	vcdp->chgBus  (c+1,(vlTOPp->v__DOT__img_pixel),8);
	vcdp->chgBus  (c+2,(vlTOPp->v__DOT__img_pixel2),8);
	vcdp->chgBus  (c+3,(vlTOPp->v__DOT__img_pixel3),8);
	vcdp->chgBus  (c+4,(vlTOPp->v__DOT__img_pixel4),8);
	vcdp->chgBus  (c+5,(vlTOPp->v__DOT__img_pixel5),8);
    }
}

void VClock::traceChgThis__3(VClock__Syms* __restrict vlSymsp, VerilatedVcd* vcdp, uint32_t code) {
    VClock* __restrict vlTOPp VL_ATTR_UNUSED = vlSymsp->TOPp;
    int c=code;
    if (0 && vcdp && c) {}  // Prevent unused
    // Body
    {
	vcdp->chgBus  (c+6,(vlTOPp->v__DOT__vcount),10);
	vcdp->chgBus  (c+7,(vlTOPp->v__DOT__hcount),11);
	vcdp->chgBus  (c+8,(vlTOPp->v__DOT__rgb),8);
	vcdp->chgBus  (c+9,(vlTOPp->v__DOT__sec),6);
	vcdp->chgBus  (c+10,(vlTOPp->v__DOT__ones),4);
	vcdp->chgBus  (c+11,(vlTOPp->v__DOT__tens),3);
	vcdp->chgBus  (c+12,(vlTOPp->v__DOT__hour),4);
	vcdp->chgBus  (c+13,(vlTOPp->v__DOT__counter),19);
	vcdp->chgBus  (c+14,(vlTOPp->v__DOT__address),16);
	vcdp->chgBus  (c+15,(vlTOPp->v__DOT__address2),16);
	vcdp->chgBus  (c+16,(vlTOPp->v__DOT__address3),16);
	vcdp->chgBus  (c+17,(vlTOPp->v__DOT__address4),16);
	vcdp->chgBus  (c+18,(vlTOPp->v__DOT__address5),16);
	vcdp->chgBus  (c+19,(vlTOPp->v__DOT__firstPos),16);
	vcdp->chgBus  (c+20,(vlTOPp->v__DOT__secondPos),16);
	vcdp->chgBus  (c+21,(vlTOPp->v__DOT__fourthPos),16);
	vcdp->chgBus  (c+22,(vlTOPp->v__DOT__fifthPos),16);
    }
}

void VClock::traceChgThis__4(VClock__Syms* __restrict vlSymsp, VerilatedVcd* vcdp, uint32_t code) {
    VClock* __restrict vlTOPp VL_ATTR_UNUSED = vlSymsp->TOPp;
    int c=code;
    if (0 && vcdp && c) {}  // Prevent unused
    // Body
    {
	vcdp->chgBit  (c+23,(vlTOPp->reset));
	vcdp->chgBit  (c+24,(vlTOPp->clk));
	vcdp->chgBit  (c+25,(vlTOPp->hora_btn));
	vcdp->chgBus  (c+26,(vlTOPp->red),3);
	vcdp->chgBus  (c+27,(vlTOPp->green),3);
	vcdp->chgBus  (c+28,(vlTOPp->blue),2);
	vcdp->chgBit  (c+29,(vlTOPp->hsync));
	vcdp->chgBit  (c+30,(vlTOPp->vsync));
    }
}
