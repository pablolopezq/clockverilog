// Verilated -*- C++ -*-
// DESCRIPTION: Verilator output: Primary design header
//
// This header should be included by all source files instantiating the design.
// The class here is then constructed to instantiate the design.
// See the Verilator manual for examples.

#ifndef _VClock_H_
#define _VClock_H_

#include "verilated.h"
#include "VClock__Inlines.h"
class VClock__Syms;
class VerilatedVcd;

//----------

VL_MODULE(VClock) {
  public:
    // CELLS
    // Public to allow access to /*verilator_public*/ items;
    // otherwise the application code can consider these internals.
    
    // PORTS
    // The application code writes and reads these signals to
    // propagate new values into/out from the Verilated model.
    VL_IN8(clk,0,0);
    VL_IN8(reset,0,0);
    VL_IN8(hora_btn,0,0);
    VL_OUT8(red,2,0);
    VL_OUT8(green,2,0);
    VL_OUT8(blue,1,0);
    VL_OUT8(hsync,0,0);
    VL_OUT8(vsync,0,0);
    
    // LOCAL SIGNALS
    // Internals; generally not touched by application code
    VL_SIG8(v__DOT__rgb,7,0);
    VL_SIG8(v__DOT__sec,5,0);
    VL_SIG8(v__DOT__ones,3,0);
    VL_SIG8(v__DOT__tens,2,0);
    VL_SIG8(v__DOT__hour,3,0);
    VL_SIG8(v__DOT__img_pixel,7,0);
    VL_SIG8(v__DOT__img_pixel2,7,0);
    VL_SIG8(v__DOT__img_pixel3,7,0);
    VL_SIG8(v__DOT__img_pixel4,7,0);
    VL_SIG8(v__DOT__img_pixel5,7,0);
    VL_SIG16(v__DOT__vcount,9,0);
    VL_SIG16(v__DOT__hcount,10,0);
    VL_SIG16(v__DOT__address,15,0);
    VL_SIG16(v__DOT__address2,15,0);
    VL_SIG16(v__DOT__address3,15,0);
    VL_SIG16(v__DOT__address4,15,0);
    VL_SIG16(v__DOT__address5,15,0);
    VL_SIG16(v__DOT__firstPos,15,0);
    VL_SIG16(v__DOT__secondPos,15,0);
    VL_SIG16(v__DOT__fourthPos,15,0);
    VL_SIG16(v__DOT__fifthPos,15,0);
    VL_SIG(v__DOT__counter,18,0);
    VL_SIG8(v__DOT__romimage__DOT__rom_content[57600],7,0);
    VL_SIG8(v__DOT__romimage2__DOT__rom_content[57600],7,0);
    VL_SIG8(v__DOT__romimage3__DOT__rom_content[57600],7,0);
    VL_SIG8(v__DOT__romimage4__DOT__rom_content[57600],7,0);
    VL_SIG8(v__DOT__romimage5__DOT__rom_content[57600],7,0);
    
    // LOCAL VARIABLES
    // Internals; generally not touched by application code
    static VL_ST_SIG16(__Vtable1_v__DOT__firstPos[16],15,0);
    static VL_ST_SIG16(__Vtable1_v__DOT__secondPos[16],15,0);
    static VL_ST_SIG16(__Vtable2_v__DOT__fifthPos[16],15,0);
    static VL_ST_SIG16(__Vtable3_v__DOT__fourthPos[8],15,0);
    VL_SIG8(__Vclklast__TOP__clk,0,0);
    //char	__VpadToAlign288053[3];
    VL_SIG(__Vm_traceActivity,31,0);
    //char	__VpadToAlign288060[4];
    VL_SIG8(__Vtablechg1[16],1,0);
    VL_SIG8(__Vtablechg2[16],0,0);
    VL_SIG8(__Vtablechg3[8],0,0);
    
    // INTERNAL VARIABLES
    // Internals; generally not touched by application code
    //char	__VpadToAlign288108[4];
    VClock__Syms*	__VlSymsp;		// Symbol table
    
    // PARAMETERS
    // Parameters marked /*verilator public*/ for use by application code
    
    // CONSTRUCTORS
  private:
    VClock& operator= (const VClock&);	///< Copying not allowed
    VClock(const VClock&);	///< Copying not allowed
  public:
    /// Construct the model; called by application code
    /// The special name  may be used to make a wrapper with a
    /// single model invisible WRT DPI scope names.
    VClock(const char* name="TOP");
    /// Destroy the model; called (often implicitly) by application code
    ~VClock();
    /// Trace signals in the model; called by application code
    void trace (VerilatedVcdC* tfp, int levels, int options=0);
    
    // USER METHODS
    
    // API METHODS
    /// Evaluate the model.  Application must call when inputs change.
    void eval();
    /// Simulation complete, run final blocks.  Application must call on completion.
    void final();
    
    // INTERNAL METHODS
  private:
    static void _eval_initial_loop(VClock__Syms* __restrict vlSymsp);
  public:
    void __Vconfigure(VClock__Syms* symsp, bool first);
  private:
    static QData	_change_request(VClock__Syms* __restrict vlSymsp);
  public:
    static void	_eval(VClock__Syms* __restrict vlSymsp);
    static void	_eval_initial(VClock__Syms* __restrict vlSymsp);
    static void	_eval_settle(VClock__Syms* __restrict vlSymsp);
    static void	_initial__TOP__1(VClock__Syms* __restrict vlSymsp);
    static void	_sequent__TOP__2(VClock__Syms* __restrict vlSymsp);
    static void	_settle__TOP__3(VClock__Syms* __restrict vlSymsp);
    static void	traceChgThis(VClock__Syms* __restrict vlSymsp, VerilatedVcd* vcdp, uint32_t code);
    static void	traceChgThis__2(VClock__Syms* __restrict vlSymsp, VerilatedVcd* vcdp, uint32_t code);
    static void	traceChgThis__3(VClock__Syms* __restrict vlSymsp, VerilatedVcd* vcdp, uint32_t code);
    static void	traceChgThis__4(VClock__Syms* __restrict vlSymsp, VerilatedVcd* vcdp, uint32_t code);
    static void	traceFullThis(VClock__Syms* __restrict vlSymsp, VerilatedVcd* vcdp, uint32_t code);
    static void	traceFullThis__1(VClock__Syms* __restrict vlSymsp, VerilatedVcd* vcdp, uint32_t code);
    static void	traceInitThis(VClock__Syms* __restrict vlSymsp, VerilatedVcd* vcdp, uint32_t code);
    static void	traceInitThis__1(VClock__Syms* __restrict vlSymsp, VerilatedVcd* vcdp, uint32_t code);
    static void traceInit (VerilatedVcd* vcdp, void* userthis, uint32_t code);
    static void traceFull (VerilatedVcd* vcdp, void* userthis, uint32_t code);
    static void traceChg  (VerilatedVcd* vcdp, void* userthis, uint32_t code);
} VL_ATTR_ALIGNED(128);

#endif  /*guard*/
