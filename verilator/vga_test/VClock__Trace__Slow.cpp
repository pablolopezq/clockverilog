// Verilated -*- C++ -*-
// DESCRIPTION: Verilator output: Tracing implementation internals
#include "verilated_vcd_c.h"
#include "VClock__Syms.h"


//======================

void VClock::trace (VerilatedVcdC* tfp, int, int) {
    tfp->spTrace()->addCallback (&VClock::traceInit, &VClock::traceFull, &VClock::traceChg, this);
}
void VClock::traceInit(VerilatedVcd* vcdp, void* userthis, uint32_t code) {
    // Callback from vcd->open()
    VClock* t=(VClock*)userthis;
    VClock__Syms* __restrict vlSymsp = t->__VlSymsp; // Setup global symbol table
    if (!Verilated::calcUnusedSigs()) vl_fatal(__FILE__,__LINE__,__FILE__,"Turning on wave traces requires Verilated::traceEverOn(true) call before time 0.");
    vcdp->scopeEscape(' ');
    t->traceInitThis (vlSymsp, vcdp, code);
    vcdp->scopeEscape('.');
}
void VClock::traceFull(VerilatedVcd* vcdp, void* userthis, uint32_t code) {
    // Callback from vcd->dump()
    VClock* t=(VClock*)userthis;
    VClock__Syms* __restrict vlSymsp = t->__VlSymsp; // Setup global symbol table
    t->traceFullThis (vlSymsp, vcdp, code);
}

//======================


void VClock::traceInitThis(VClock__Syms* __restrict vlSymsp, VerilatedVcd* vcdp, uint32_t code) {
    VClock* __restrict vlTOPp VL_ATTR_UNUSED = vlSymsp->TOPp;
    int c=code;
    if (0 && vcdp && c) {}  // Prevent unused
    vcdp->module(vlSymsp->name()); // Setup signal names
    // Body
    {
	vlTOPp->traceInitThis__1(vlSymsp, vcdp, code);
    }
}

void VClock::traceFullThis(VClock__Syms* __restrict vlSymsp, VerilatedVcd* vcdp, uint32_t code) {
    VClock* __restrict vlTOPp VL_ATTR_UNUSED = vlSymsp->TOPp;
    int c=code;
    if (0 && vcdp && c) {}  // Prevent unused
    // Body
    {
	vlTOPp->traceFullThis__1(vlSymsp, vcdp, code);
    }
    // Final
    vlTOPp->__Vm_traceActivity = 0U;
}

void VClock::traceInitThis__1(VClock__Syms* __restrict vlSymsp, VerilatedVcd* vcdp, uint32_t code) {
    VClock* __restrict vlTOPp VL_ATTR_UNUSED = vlSymsp->TOPp;
    int c=code;
    if (0 && vcdp && c) {}  // Prevent unused
    // Body
    {
	vcdp->declBit  (c+23,"reset",-1);
	vcdp->declBit  (c+24,"clk",-1);
	vcdp->declBit  (c+25,"hora_btn",-1);
	vcdp->declBus  (c+26,"red",-1,2,0);
	vcdp->declBus  (c+27,"green",-1,2,0);
	vcdp->declBus  (c+28,"blue",-1,1,0);
	vcdp->declBit  (c+29,"hsync",-1);
	vcdp->declBit  (c+30,"vsync",-1);
	vcdp->declBit  (c+23,"v reset",-1);
	vcdp->declBit  (c+24,"v clk",-1);
	vcdp->declBit  (c+25,"v hora_btn",-1);
	vcdp->declBus  (c+26,"v red",-1,2,0);
	vcdp->declBus  (c+27,"v green",-1,2,0);
	vcdp->declBus  (c+28,"v blue",-1,1,0);
	vcdp->declBit  (c+29,"v hsync",-1);
	vcdp->declBit  (c+30,"v vsync",-1);
	vcdp->declBus  (c+6,"v vcount",-1,9,0);
	vcdp->declBus  (c+7,"v hcount",-1,10,0);
	vcdp->declBus  (c+8,"v rgb",-1,7,0);
	vcdp->declBus  (c+9,"v sec",-1,5,0);
	vcdp->declBus  (c+10,"v ones",-1,3,0);
	vcdp->declBus  (c+11,"v tens",-1,2,0);
	vcdp->declBus  (c+12,"v hour",-1,3,0);
	vcdp->declBus  (c+13,"v counter",-1,18,0);
	vcdp->declBus  (c+1,"v color",-1,7,0);
	vcdp->declBus  (c+2,"v color2",-1,7,0);
	vcdp->declBus  (c+3,"v color3",-1,7,0);
	vcdp->declBus  (c+4,"v color4",-1,7,0);
	vcdp->declBus  (c+5,"v color5",-1,7,0);
	vcdp->declBus  (c+14,"v address",-1,15,0);
	vcdp->declBus  (c+1,"v img_pixel",-1,7,0);
	vcdp->declBus  (c+15,"v address2",-1,15,0);
	vcdp->declBus  (c+2,"v img_pixel2",-1,7,0);
	vcdp->declBus  (c+16,"v address3",-1,15,0);
	vcdp->declBus  (c+3,"v img_pixel3",-1,7,0);
	vcdp->declBus  (c+17,"v address4",-1,15,0);
	vcdp->declBus  (c+4,"v img_pixel4",-1,7,0);
	vcdp->declBus  (c+18,"v address5",-1,15,0);
	vcdp->declBus  (c+5,"v img_pixel5",-1,7,0);
	vcdp->declBus  (c+19,"v firstPos",-1,15,0);
	vcdp->declBus  (c+20,"v secondPos",-1,15,0);
	vcdp->declBus  (c+21,"v fourthPos",-1,15,0);
	vcdp->declBus  (c+22,"v fifthPos",-1,15,0);
	vcdp->declBus  (c+14,"v romimage address",-1,15,0);
	vcdp->declBus  (c+1,"v romimage data",-1,7,0);
	// Tracing: v romimage rom_content // Ignored: Wide memory > --trace-max-array ents at ../vsrc//ImageROM.v:5
	vcdp->declBus  (c+15,"v romimage2 address",-1,15,0);
	vcdp->declBus  (c+2,"v romimage2 data",-1,7,0);
	// Tracing: v romimage2 rom_content // Ignored: Wide memory > --trace-max-array ents at ../vsrc//ImageROM.v:5
	vcdp->declBus  (c+16,"v romimage3 address",-1,15,0);
	vcdp->declBus  (c+3,"v romimage3 data",-1,7,0);
	// Tracing: v romimage3 rom_content // Ignored: Wide memory > --trace-max-array ents at ../vsrc//ImageROM.v:5
	vcdp->declBus  (c+17,"v romimage4 address",-1,15,0);
	vcdp->declBus  (c+4,"v romimage4 data",-1,7,0);
	// Tracing: v romimage4 rom_content // Ignored: Wide memory > --trace-max-array ents at ../vsrc//ImageROM.v:5
	vcdp->declBus  (c+18,"v romimage5 address",-1,15,0);
	vcdp->declBus  (c+5,"v romimage5 data",-1,7,0);
	// Tracing: v romimage5 rom_content // Ignored: Wide memory > --trace-max-array ents at ../vsrc//ImageROM.v:5
    }
}

void VClock::traceFullThis__1(VClock__Syms* __restrict vlSymsp, VerilatedVcd* vcdp, uint32_t code) {
    VClock* __restrict vlTOPp VL_ATTR_UNUSED = vlSymsp->TOPp;
    int c=code;
    if (0 && vcdp && c) {}  // Prevent unused
    // Body
    {
	vcdp->fullBus  (c+1,(vlTOPp->v__DOT__img_pixel),8);
	vcdp->fullBus  (c+2,(vlTOPp->v__DOT__img_pixel2),8);
	vcdp->fullBus  (c+3,(vlTOPp->v__DOT__img_pixel3),8);
	vcdp->fullBus  (c+4,(vlTOPp->v__DOT__img_pixel4),8);
	vcdp->fullBus  (c+5,(vlTOPp->v__DOT__img_pixel5),8);
	vcdp->fullBus  (c+6,(vlTOPp->v__DOT__vcount),10);
	vcdp->fullBus  (c+7,(vlTOPp->v__DOT__hcount),11);
	vcdp->fullBus  (c+8,(vlTOPp->v__DOT__rgb),8);
	vcdp->fullBus  (c+9,(vlTOPp->v__DOT__sec),6);
	vcdp->fullBus  (c+10,(vlTOPp->v__DOT__ones),4);
	vcdp->fullBus  (c+11,(vlTOPp->v__DOT__tens),3);
	vcdp->fullBus  (c+12,(vlTOPp->v__DOT__hour),4);
	vcdp->fullBus  (c+13,(vlTOPp->v__DOT__counter),19);
	vcdp->fullBus  (c+14,(vlTOPp->v__DOT__address),16);
	vcdp->fullBus  (c+15,(vlTOPp->v__DOT__address2),16);
	vcdp->fullBus  (c+16,(vlTOPp->v__DOT__address3),16);
	vcdp->fullBus  (c+17,(vlTOPp->v__DOT__address4),16);
	vcdp->fullBus  (c+18,(vlTOPp->v__DOT__address5),16);
	vcdp->fullBus  (c+19,(vlTOPp->v__DOT__firstPos),16);
	vcdp->fullBus  (c+20,(vlTOPp->v__DOT__secondPos),16);
	vcdp->fullBus  (c+21,(vlTOPp->v__DOT__fourthPos),16);
	vcdp->fullBus  (c+22,(vlTOPp->v__DOT__fifthPos),16);
	vcdp->fullBit  (c+23,(vlTOPp->reset));
	vcdp->fullBit  (c+24,(vlTOPp->clk));
	vcdp->fullBit  (c+25,(vlTOPp->hora_btn));
	vcdp->fullBus  (c+26,(vlTOPp->red),3);
	vcdp->fullBus  (c+27,(vlTOPp->green),3);
	vcdp->fullBus  (c+28,(vlTOPp->blue),2);
	vcdp->fullBit  (c+29,(vlTOPp->hsync));
	vcdp->fullBit  (c+30,(vlTOPp->vsync));
    }
}
